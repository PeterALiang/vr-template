﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ControllerReticle : MonoBehaviour
{
	private Canvas _canvas;
	private Image _reticle;
	private Image _gazeTimer;
	private Image _gazeTimerFill;
	private CanvasGroup _gazeCanvasGroup;

	private float _gazeTimestamp;

	void Start()
	{
		_canvas = GetComponentInChildren<Canvas>();

		if (!_canvas)
		{
			_canvas = new GameObject("CameraCanvas").AddComponent<Canvas>();
			_canvas.gameObject.AddComponent<CanvasScaler>();
			_canvas.gameObject.AddComponent<CanvasGroup>().alpha = 0.75f;
			_canvas.transform.SetParent(transform);
			_canvas.renderMode = RenderMode.ScreenSpaceCamera;

			if (!(_canvas.worldCamera = GetComponent<Camera>()))
			{
				_canvas.worldCamera = gameObject.AddComponent<Camera>();
				_canvas.worldCamera.enabled = false;
			}
			
			_canvas.planeDistance = 4;
			_canvas.renderMode = RenderMode.WorldSpace;
			_canvas.sortingOrder = 1;

			foreach (var canvas in FindObjectsOfType<Canvas>())
			{
				if (canvas.renderMode == RenderMode.WorldSpace)
					canvas.worldCamera = _canvas.worldCamera;
			}

			if (!_canvas.worldCamera.GetComponent<PhysicsRaycaster>())
				_canvas.worldCamera.gameObject.AddComponent<PhysicsRaycaster>();
		}
		else if (_canvas.worldCamera && !_canvas.worldCamera.GetComponent<PhysicsRaycaster>())
		{
			foreach (var canvas in FindObjectsOfType<Canvas>())
			{
				if (canvas.renderMode == RenderMode.WorldSpace)
					canvas.worldCamera = _canvas.worldCamera;
			}

			_canvas.worldCamera.gameObject.AddComponent<PhysicsRaycaster>();
		}


		Material uiMat = new Material(Shader.Find("UI/Default"));
		uiMat.SetInt("unity_GUIZTestMode", (int)UnityEngine.Rendering.CompareFunction.Always);


		Transform searchResult;

		if (!(searchResult = _canvas.transform.Find("Reticle")) || searchResult && !(_reticle = searchResult.GetComponent<Image>()))
			_reticle = MakeCircleImage("Reticle", _canvas.transform, new Vector2(2, 2), 64);
		else if (!_reticle.sprite)
		{
			Destroy(_reticle.gameObject);
			_reticle = MakeCircleImage("Reticle", _canvas.transform, new Vector2(2, 2), 64);
		}

		_reticle.material = uiMat;

		if (!(searchResult = _canvas.transform.Find("GazeTimer")) || searchResult && !(_gazeTimer = searchResult.GetComponent<Image>()))
			_gazeTimer = MakeCircleImage("GazeTimer", _canvas.transform, new Vector2(6, 6), 128, 0.125f);
		else if (!_reticle.sprite)
		{
			Destroy(_gazeTimer.gameObject);
			_gazeTimer = MakeCircleImage("GazeTimer", _canvas.transform, new Vector2(6, 6), 128, 0.125f);
		}

		if (!(_gazeCanvasGroup = _gazeTimer.gameObject.GetComponent<CanvasGroup>()))
		{
			_gazeCanvasGroup = _gazeTimer.gameObject.AddComponent<CanvasGroup>();
			_gazeCanvasGroup.alpha = 0;
		}
		_gazeTimer.color = new Color(1, 1, 1, 0.33f);
		_gazeTimer.material = uiMat;

		if (!(searchResult = _gazeTimer.transform.Find("GazeFill")) || searchResult && !(_gazeTimerFill = searchResult.GetComponent<Image>()))
			_gazeTimerFill = MakeCircleImage("GazeFill", _gazeTimer.transform, new Vector2(6, 6), 128, 0.125f);
		else if (!_gazeTimerFill.sprite)
		{
			Destroy(_gazeTimerFill.gameObject);
			_gazeTimerFill = MakeCircleImage("GazeFill", _gazeTimer.transform, new Vector2(6, 6), 128, 0.125f);
		}

		_gazeTimerFill.type = Image.Type.Filled;
		_gazeTimerFill.fillMethod = Image.FillMethod.Radial360;
		_gazeTimerFill.fillAmount = 0.5f;
		_gazeTimerFill.material = uiMat;
	}

	void Update()
	{
		float targetDist = _canvas.planeDistance;

		if (XRInputModule.Instance.mode == XRInputModule.Mode.Gaze)
		{
			if (XRInputModule.Instance.ObjectUnderAimer)
			{
				if (_gazeCanvasGroup.alpha < 1)
					_gazeCanvasGroup.alpha += Time.deltaTime / 0.25f;
				else
					_gazeCanvasGroup.alpha = 1;

				if (XRInputModule.Instance.GazeTimestamp != float.MaxValue)
					_gazeTimerFill.fillAmount = 1 - (XRInputModule.Instance.GazeTimestamp - Time.realtimeSinceStartup) / XRInputModule.Instance.GazeTimeInSeconds;
				else
					_gazeTimerFill.fillAmount = 1;
			}
			else
			{
				if (_gazeCanvasGroup.alpha > 0)
					_gazeCanvasGroup.alpha -= Time.deltaTime / 0.25f;
				else
					_gazeCanvasGroup.alpha = 0;
			}
		}
		else
		{
			if (_gazeCanvasGroup.alpha > 0)
				_gazeCanvasGroup.alpha -= Time.deltaTime / 0.25f;
			else
				_gazeCanvasGroup.alpha = 0;
		}

		targetDist = XRInputModule.Instance.RaycastDist;

		_canvas.renderMode = RenderMode.ScreenSpaceCamera;
		_canvas.planeDistance = Mathf.Lerp(_canvas.planeDistance, targetDist, 0.33f);
		_canvas.renderMode = RenderMode.WorldSpace;
	}

	public Image MakeCircleImage(string name, Transform parent, Vector2 sizeDelta, int texSize, float borderWidth = 0)
	{
		Image newImage = new GameObject(name).AddComponent<Image>();
		newImage.transform.SetParent(parent);
		newImage.transform.localPosition = Vector3.zero;
		newImage.transform.localRotation = Quaternion.identity;
		newImage.raycastTarget = false;
		newImage.GetComponent<RectTransform>().sizeDelta = sizeDelta;

		Texture2D tex = new Texture2D(texSize, texSize, TextureFormat.ARGB32, true);

		if (borderWidth == 0)
			Circle(tex, texSize / 2, texSize / 2, texSize / 4, Color.white);
		else
			CircleBorder(tex, texSize / 2, texSize / 2, texSize / 4, Mathf.RoundToInt(texSize * borderWidth), Color.white);
		
		//GausianBlur(tex, false, new Vector2(4, 4));
		tex.filterMode = FilterMode.Trilinear;

		newImage.sprite = Sprite.Create(tex, new Rect(0, 0, texSize, texSize), Vector2.zero);
		return newImage;
	}

	public void Circle(Texture2D tex, int cx, int cy, int r, Color col)
	{
		int x, y, px, nx, py, ny, d;
		Color32[] tempArray = tex.GetPixels32();

		// Set texture to transparent
		for (var i = 0; i < tempArray.Length; ++i)
			tempArray[i] = new Color32(255, 255, 255, 0);

		tex.SetPixels32(tempArray);
		tex.Apply();

		// Draw circle on texture
		for (x = 0; x <= r; x++)
		{
			d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));
			
			for (y = 0; y <= d; y++)
			{
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;

				tempArray[py * tex.width + px] = col;
				tempArray[py * tex.width + nx] = col;
				tempArray[ny * tex.width + px] = col;
				tempArray[ny * tex.width + nx] = col;
			}
		}
		tex.SetPixels32(tempArray);
		tex.Apply();
	}

	public void CircleBorder(Texture2D tex, int cx, int cy, int r, int w, Color col)
	{
		int x, y, px, nx, py, ny, d;
		Color32[] tempArray = tex.GetPixels32();

		// Set texture to transparent
		for (var i = 0; i < tempArray.Length; ++i)
			tempArray[i] = new Color32(255, 255, 255, 0);

		tex.SetPixels32(tempArray);
		tex.Apply();

		// Draw circle on texture
		for (x = 0; x <= r + w; x++)
		{
			d = (int)Mathf.Floor(Mathf.Sqrt((r + w) * (r + w) - x * x));
			for (y = 0; y <= d; y++)
			{
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;

				tempArray[py * tex.width + px] = col;
				tempArray[py * tex.width + nx] = col;
				tempArray[ny * tex.width + px] = col;
				tempArray[ny * tex.width + nx] = col;
			}
		}
		tex.SetPixels32(tempArray);
		tex.Apply();

		// Clear the interior of the circle
		for (x = 0; x <= r; x++)
		{
			d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));
			for (y = 0; y <= d; y++)
			{
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;

				tempArray[py * tex.width + px] = new Color32(255, 255, 255, 0);
				tempArray[py * tex.width + nx] = new Color32(255, 255, 255, 0);
				tempArray[ny * tex.width + px] = new Color32(255, 255, 255, 0);
				tempArray[ny * tex.width + nx] = new Color32(255, 255, 255, 0);
			}
		}
		tex.SetPixels32(tempArray);
		tex.Apply();
	}

	private Color Average(Vector2 blurSize, Texture2D tex, int xpixel, int ypixel)
	{
		List<Color> colorList = new List<Color>();

		for (int x = xpixel - (Mathf.RoundToInt(blurSize.x) / 2); x < xpixel + (Mathf.RoundToInt(blurSize.x) / 2); x++)
		{
			for (int y = ypixel - (Mathf.RoundToInt(blurSize.y) / 2); y < ypixel + (Mathf.RoundToInt(blurSize.y) / 2); y++)
			{
				if (x > 0 && x < tex.width && y > 0 && y < tex.height)
					colorList.Add(tex.GetPixel(x, y));
			}
		}
		
		float a = 0, r = 0, g = 0, b = 0;

		foreach (var thisColor in colorList)
		{
			a += thisColor.a;
			r += thisColor.r;
			g += thisColor.g;
			b += thisColor.b;
		}

		return new Color(r / colorList.Count, g / colorList.Count, b / colorList.Count, a / colorList.Count);
	}

	private void GausianBlur(Texture2D tex, bool alphaEdgesOnly, Vector2 blurSize)
	{
		for (int ypixel = 0; ypixel < tex.width - 1; ypixel++)
		{
			for (int xpixel = 0; xpixel < tex.height - 1; xpixel++)
			{
				if (!alphaEdgesOnly)
				{
					tex.SetPixel(xpixel, ypixel, Average(blurSize, tex, xpixel, ypixel));
				}
				else if (tex.GetPixel(xpixel, ypixel).a != 1)
				{
					tex.SetPixel(xpixel, ypixel, Average(blurSize, tex, xpixel, ypixel));
				}
			}
		}

		tex.Apply();
	}
}
