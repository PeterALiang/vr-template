﻿
// Gaze Input Module by Peter Koch <peterept@gmail.com>
// Credit Chris Trueman
// Sourced from - http://forum.unity3d.com/threads/use-reticle-like-mouse-for-worldspace-uis.295271/
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using UnityEngine.XR;

// To use:
// 1. Drag onto your EventSystem game object.
// 2. Disable any other Input Modules (eg: StandaloneInputModule & TouchInputModule) as they will fight over selections.
// 3. Make sure your Canvas is in world space and has a GraphicRaycaster (should by default).
// 4. If you have multiple cameras then make sure to drag your VR (center eye) camera into the canvas.
public class XRInputModule : PointerInputModule
{
	[System.Flags]
	public enum Mode
    { 
        Click,
        Gaze,
        ClickAndGaze,
        Controller
    };

	public Mode mode;

	public static XRInputModule Instance;

	[Header("Click Settings")]
	public string ClickInputName = "Fire1";
	[Header("Gaze Settings")]
	public float GazeTimeInSeconds = 2f;
	public float GazeTimestamp { get; set; }

	public float RaycastDist { get; set; }
	public GameObject ObjectUnderAimer { get; set; }

	private GameObject _currentLookAtHandler;

	protected override void Start()
	{
		base.Start();
		Instance = this;
		RaycastDist = 4;

        if (mode == Mode.Gaze || mode == Mode.ClickAndGaze)
        {
            if (!FindObjectOfType<GazeReticle>())
                Camera.main.gameObject.AddComponent<GazeReticle>();
        }
        else if (mode == Mode.Controller)
        {
            if (XRSettings.enabled)
            {
                if (XRSettings.loadedDeviceName.Equals("daydream"))
                {
                    Type gvrControllerType = Type.GetType("GvrTrackedController");
                    if (gvrControllerType != null)
                    {
                        // We're not going to allow automating the creation of this for now.
                        MonoBehaviour controller = (MonoBehaviour)FindObjectOfType(Type.GetType("GvrTrackedController"));
                        
                        if (controller)
                        {
                            //Disable GoogleVR's own laser renderer.
                            Type.GetType("GvrTrackedController").GetProperty("IsLaserVisualEnabled").SetValue(controller, false, null);

                            //Add a GvrControllerInput if there isn't one already. If gvrControllerType isn't null, we can safely assume this exists.
                            if (!FindObjectOfType(Type.GetType("GvrControllerInput")))
                                new GameObject("GvrControllerMain").AddComponent(Type.GetType("GvrControllerInput"));
                            
                            if (controller.transform.parent == null || !controller.transform.parent.Find(Camera.main.name))
                            {
                                Transform parent = new GameObject("GvrController").transform;

                                parent.position = Camera.main.transform.position;
                                parent.rotation = Camera.main.transform.rotation;

                                controller.transform.parent = parent;
                            }

                            controller.gameObject.AddComponent<ControllerReticle>();
                        }
                    }
                }
                else if (XRSettings.loadedDeviceName.Equals("oculus") && Application.platform == RuntimePlatform.Android)
                {

                }
                else if (XRSettings.loadedDeviceName.Equals("oculus") && Application.platform != RuntimePlatform.Android)
                {

                }
                else if (XRSettings.loadedDeviceName.Equals("openvr"))
                {

                }
            }
        }
	}

	public override void Process()
	{
		Cursor.lockState = CursorLockMode.None;

		bool released = Input.GetButtonUp(ClickInputName);

		PointerEventData pointer = HandleLook();

		ProcessInteraction(pointer);

		if (!released)
			ProcessMove(pointer);
		else
			RemovePointerData(pointer);

		Cursor.lockState = CursorLockMode.Locked;
	}

	PointerEventData HandleLook()
	{
		PointerEventData pointerData;

		//Not certain on the use of this.
		//I know that -1 is the mouse and anything positive would be a finger/touch, 0 being the first finger, 1 beign the second and so one till the system limit is reached.
		//So that is the reason I choose -2.

		GetPointerData(-2, out pointerData, true);

		pointerData.Reset();

		// fake a pointer always being at the center of the screen
		if (XRSettings.enabled)
			pointerData.position = new Vector2(XRSettings.eyeTextureWidth / 2, XRSettings.eyeTextureHeight / 2);
		else
			pointerData.position = new Vector2(Screen.width / 2, Screen.height / 2);

		eventSystem.RaycastAll(pointerData, m_RaycastResultCache);
		var raycast = FindFirstRaycast(m_RaycastResultCache);
		pointerData.pointerCurrentRaycast = raycast;
		m_RaycastResultCache.Clear();
		return pointerData;
	}

	private void ProcessInteraction(PointerEventData pointer)
	{
		var currentOverGo = pointer.pointerCurrentRaycast.gameObject;

		if (currentOverGo)
			RaycastDist = pointer.pointerCurrentRaycast.distance;

		ObjectUnderAimer = ExecuteEvents.GetEventHandler<ISubmitHandler>(currentOverGo);//we only want objects that we can submit on.

		if (ObjectUnderAimer != null)
		{
			// if the ui receiver has changed, reset the gaze delay timer
			GameObject handler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(ObjectUnderAimer);
			if (_currentLookAtHandler != handler)
			{
				_currentLookAtHandler = handler;
				GazeTimestamp = Time.realtimeSinceStartup + GazeTimeInSeconds;
			}

			// if we have a handler and it's time to click, do it now
			if (_currentLookAtHandler != null &&
				(mode == Mode.Gaze && Time.realtimeSinceStartup > GazeTimestamp) ||
				((mode == Mode.Click || (mode == Mode.Controller)) && Input.GetButtonDown(ClickInputName)) ||
				(mode == Mode.ClickAndGaze && (Time.realtimeSinceStartup > GazeTimestamp || Input.GetButtonDown(ClickInputName))))
			{
				pointer.eligibleForClick = true;
				pointer.delta = Vector2.zero;
				pointer.pressPosition = pointer.position;
				pointer.pointerPressRaycast = pointer.pointerCurrentRaycast;

				// search for the control that will receive the press
				// if we can't find a press handler set the press
				// handler to be what would receive a click.
				var newPressed = ExecuteEvents.ExecuteHierarchy(currentOverGo, pointer, ExecuteEvents.submitHandler);

				// didnt find a press handler... search for a click handler
				if (newPressed == null)
				{
					newPressed = ExecuteEvents.ExecuteHierarchy(currentOverGo, pointer, ExecuteEvents.pointerClickHandler);
					if (newPressed == null)
						newPressed = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGo);
				}
				else
				{
					pointer.eligibleForClick = false;
				}

				if (newPressed != pointer.pointerPress)
				{
					pointer.pointerPress = newPressed;
					pointer.rawPointerPress = currentOverGo;
					pointer.clickCount = 0;
				}

				GazeTimestamp = float.MaxValue;
			}
		}
		else
		{
			_currentLookAtHandler = null;
		}
	}

	public override void ActivateModule()
	{
		StandaloneInputModule StandAloneSystem = GetComponent<StandaloneInputModule>();

		if (StandAloneSystem != null && StandAloneSystem.enabled)
		{
			Debug.LogError("Gaze Input Module is incompatible with the StandAloneInputSystem, " +
				"please remove it from the Event System in this scene or disable it when this module is in use");
		}
	}

	public override void DeactivateModule()
	{
		base.DeactivateModule();
		ClearSelection();
	}
}