﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Indicator : MonoBehaviour
{
    public Transform startTarget;
    
    public Image indicator;

    public IndicatorType indicatorType;
    public Vector2 dimensions;

    private float angle;

    public Transform target { get; set; }
    public bool active { get; set; }
    public bool visible { get; set; }

    private RectTransform indicatorRect;
    private CanvasGroup indicatorCanvas;
    private Vector2 borderAxis;

    void Start()
    {
        indicatorRect = indicator.GetComponent<RectTransform>();
        indicatorCanvas = indicator.GetComponent<CanvasGroup>();

        active = true;

        if (startTarget)
            target = startTarget;
    }

    void Update()
    {
        if (target && active && visible)
            indicatorCanvas.alpha += Time.deltaTime * 10;
        else
        {
            indicatorCanvas.alpha -= Time.deltaTime * 10;
            
            if (!target || !active && indicatorCanvas.alpha <= 0)
                return;
        }
        
        if (CanCameraSeePoint(Camera.main, target.position))
        {
            visible = false;
        }
        else
        {
            visible = true;

            angle = ClampAngle(-AngleFromTarget() - 90);

            if (indicatorType == IndicatorType.Circular)
            {
                indicatorRect.anchoredPosition = GetPointOnEllipse(dimensions.x, dimensions.y, angle);
                indicatorRect.localEulerAngles = Vector3.forward * -angle;
            }
            else if (indicatorType == IndicatorType.Rectangular)
            {
                indicatorRect.anchoredPosition = GetPointOnRect(dimensions.x, dimensions.y, angle);
                indicatorRect.localEulerAngles = Vector3.forward * BorderDirection();
            }
        }
    }

    // https://forum.unity.com/threads/point-in-camera-view.72523/#post-464141
    bool CanCameraSeePoint(Camera camera, Vector3 point)
    {
        Vector3 viewportPoint = camera.WorldToViewportPoint(point);
        return (viewportPoint.z > 0 && (new Rect(0, 0, 1, 1)).Contains(viewportPoint));
    }

    float AngleFromTarget()
    {
        Vector3 toTarget = (target.position - Camera.main.transform.position).normalized;

        if (Vector3.Dot(toTarget, transform.forward) > 0)
        {
            // Target is in front of this game object
            Vector2 centre = Camera.main.ViewportToScreenPoint(Vector2.one / 2f);
            Vector2 targetDir = (Vector2)Camera.main.WorldToScreenPoint(target.position) - centre;
            return Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg + 180;
        }
        else
        {
            // Target is behind of this game object
            Vector2 centre = Camera.main.ViewportToScreenPoint(Vector2.one / 2f);
            Vector2 targetPoint = Vector2.one - (Vector2)Camera.main.WorldToViewportPoint(target.position);
            Vector2 targetDir = (Vector2)Camera.main.ViewportToScreenPoint(targetPoint) - centre;
            return Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg + 180;
        }
    }

    float ClampAngle(float value)
    {
        if (value > 360)
            return value - 360;
        else if (value < 0)
            return value + 360;
        else
            return value;
    }

    float BorderDirection()
    {
        borderAxis.x = GetPointOnRect(dimensions.x, dimensions.y, angle).x * 2;
        borderAxis.y = GetPointOnRect(dimensions.x, dimensions.y, angle).y * 2;

        if (borderAxis.y == dimensions.y)
            return 0;
        else if (borderAxis.x == -dimensions.x)
            return 90;
        else if (borderAxis.y == -dimensions.y)
            return 180;
        else if (borderAxis.x == dimensions.x)
            return 270;
        else
            return -1;
    }

    Vector2 GetPointOnEllipse(float width, float height, float angle)
    {
        angle = Mathf.Deg2Rad * -(angle - 90);

        Vector2 point;
        point.x = transform.position.x + Mathf.Cos(angle) * width / 2;
        point.y = transform.position.y + Mathf.Sin(angle) * height / 2;

        return point;
    }

    Vector2 GetPointOnRect(float width, float height, float angle)
    {
        angle = Mathf.Deg2Rad * -(angle - 90);

        float sine = Mathf.Sin(angle);
        float cosine = Mathf.Cos(angle);

        float dy = sine > 0 ? height / 2 : height / -2;
        float dx = cosine > 0 ? width / 2 : width / -2;

        if (Mathf.Abs(dx * sine) < Mathf.Abs(dy * cosine))
            dy = (dx * sine) / cosine;
        else
            dx = (dy * cosine) / sine;

        return new Vector2(dx, dy);
    }
}

public enum IndicatorType { Circular, Rectangular }
