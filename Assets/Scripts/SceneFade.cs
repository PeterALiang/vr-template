﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SceneFade : MonoBehaviour
{
	public Color32 FadeColour = Color.black;
	public float StartFadeTime = 2, EndFadeTime = 2;
	public bool FadeAudio = true;

	public bool Initialised { get; set; }
	public bool Fading { get; set; }
	
	private AudioSource[] _audioSources;
	private float[] _audioVolumes;
	private Canvas _canvas;
	private CanvasGroup _opacityControl;
	private Image _overlay;
	
	// Use this for initialization
	void Start()
	{
		Initialised = false;
		GenerateCanvas();

		if (StartFadeTime > 0)
			StartCoroutine(Fade(false));
	}

	IEnumerator Fade(bool fadeOut, string sceneName = null)
	{
		Fading = true;

		if (fadeOut)
		{
			if (EndFadeTime > 0)
			{
				if (FadeAudio)
				{
					_audioSources = FindObjectsOfType<AudioSource>();
					_audioVolumes = new float[_audioSources.Length];

					for (int i = 0; i < _audioSources.Length; i++)
					{
						_audioVolumes[i] = _audioSources[i].volume;
					}
				}
				
				for (float i = 0; i < 1; i += Time.deltaTime / EndFadeTime)
				{
					if (FadeAudio && _audioSources.Length > 0)
					{
						for (int j = 0; j < _audioSources.Length; j++)
						{
							if (_audioSources[j])
								_audioSources[j].volume = (1 - i) * _audioVolumes[j];
						}
					}
					
					_opacityControl.alpha = Mathf.SmoothStep(0, 1, i);
					yield return null;
				}

				if (FadeAudio && _audioSources.Length > 0)
				{
					foreach (var audio in _audioSources)
					{
						if (audio)
							audio.volume = 0;
					}
				}

				_opacityControl.alpha = 1;
			}
		}
		else
		{
			for (float i = 0; i < 1; i += Time.deltaTime / StartFadeTime)
			{
				_opacityControl.alpha = Mathf.SmoothStep(1, 0, i);
				yield return null;
			}

			_opacityControl.alpha = 0;
		}

		Fading = false;

		if (!string.IsNullOrEmpty(sceneName))
			SceneManager.LoadScene(sceneName);
	}

	public void FadeOut(string sceneName)
	{
		StartCoroutine(Fade(true, sceneName));
	}

	public void FadeOut(int sceneIndex)
	{
		StartCoroutine(Fade(true, SceneUtility.GetScenePathByBuildIndex(sceneIndex)));
	}

	void GenerateCanvas()
	{
		_canvas = new GameObject("ScreenCanvas").AddComponent<Canvas>();
		_canvas.gameObject.AddComponent<CanvasScaler>();
		_canvas.transform.SetParent(transform);
		_canvas.renderMode = RenderMode.ScreenSpaceCamera;
		_canvas.worldCamera = Camera.main;
		_canvas.planeDistance = 4;
		_canvas.sortingOrder = 2;

		_opacityControl = _canvas.gameObject.AddComponent<CanvasGroup>();
		_opacityControl.interactable = false;
		_opacityControl.blocksRaycasts = false;
		_opacityControl.alpha = (StartFadeTime > 0) ? 1 : 0;

		Image _overlay = new GameObject(name).AddComponent<Image>();
		_overlay.transform.SetParent(_canvas.transform);
		_overlay.transform.localPosition = Vector3.zero;
		_overlay.transform.localRotation = Quaternion.identity;
		_overlay.raycastTarget = false;

		Material uiMat = new Material(Shader.Find("UI/Default"));
		uiMat.SetInt("unity_GUIZTestMode", (int)UnityEngine.Rendering.CompareFunction.Always);
		_overlay.material = uiMat;

		RectTransform overlayRect = _overlay.GetComponent<RectTransform>();
		overlayRect.anchorMin = Vector2.zero;
		overlayRect.anchorMax = Vector2.one;
		overlayRect.sizeDelta = Vector2.zero;

		Texture2D tex = new Texture2D(1, 1);
		tex.SetPixels32(new Color32[] { FadeColour });
		tex.Apply();
		tex.filterMode = FilterMode.Point;

		_overlay.sprite = Sprite.Create(tex, new Rect(0, 0, 1, 1), Vector2.zero);

		int targetLayer = _canvas.worldCamera.gameObject.layer;
		_canvas.gameObject.layer = targetLayer;

		foreach (Transform child in _canvas.transform)
			child.gameObject.layer = targetLayer;

		Initialised = true;
	}
}